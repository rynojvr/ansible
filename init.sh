#!/bin/bash

# TODO: Improve UI; see pihole script
# TODO: Options to choose role during init

if [ "$EUID" -ne 0 ]; then
    echo "Must run as root"
    exit
fi

echo "[i] Updating apt sources"
sudo apt -q update &>/dev/null;

# echo "[i] Removing Snap firefox"
# sudo snap remove firefox

echo "[i] Installing prereq"
sudo apt -q install whois &>/dev/null;

if [ ! -f /root/.ansible_user_pwd ]; then
    echo "Enter password for local user account:"
    sudo sh -c 'mkpasswd --method=SHA-512 | tr -d "\n" > /root/.ansible_user_pwd'
fi

if [ "$HOSTNAME" = debian-fw ]; then
    if [ ! -f /root/.pihole_web_pwd ]; then
        python3 -c 'import hashlib; h = hashlib.sha256(hashlib.sha256(input("PiHole Web Password: ").encode("utf-8")).hexdigest().encode("utf-8")).hexdigest(); open("/root/.pihole_web_pwd", "w").write(h)' 
    fi
fi

conf_n2n="[n2n]
;node_id=NUM
;n2n_role=[edge|supernode|both]
;edge_pass=PASSWORD
;edge_service=[true|false]
;edge_send_routes=[true|false]
"

init_n2n() {
    while true; do
        read -p "Enter Node ID: " node_id

	if [[ $node_id =~ ^[0-9]+$ ]]; then
            if (( node_id >= 1 && node_id <= 254 )); then
                break
            fi
        fi
        echo "Enter number in range [1, 254]"
    done
    
    while true; do
        read -p "Role (Edge/Supernode/Both)? (e/s/b) " n2n_role
	case $n2n_role in
            [eE] ) n2n_role='edge';
	        break;;
            [sS] ) n2n_role='supernode';
	        break;;
            [bB] ) n2n_role='both';
            break;;
            * ) echo Invalid Response;;
        esac
    done

    # TODO: Enable Supernode config
    
    while true; do
        read -p "Enable Edge as a Service on boot? (y/n) " edge_service
        case $edge_service in
            [yY] ) edge_service=true;
                break;;
            [nN] ) edge_service=false;
                break;;
            * ) echo "Invalid response";;
        esac
    done

    read -p "Password for Edge Service? " edge_key

    while true; do
        read -p "Send routing updates? (y/n) " edge_send_routes
	case $edge_send_routes in 
            [yY] ) edge_send_routes=true;
                break;;
            [nN] ) edge_send_routes=false;
                break;;
            * ) echo "Invalid response";;
        esac
    done

    conf_n2n="[n2n]
node_id=$node_id
n2n_role=$n2n_role
edge_pass=$edge_key
edge_service=$edge_service
edge_send_routes=$edge_send_routes
"
}

initialize_config() {
    echo "Initializing config."

    read -p "Enable n2n? (y/n) " yn
    case $yn in
        [yY] ) init_n2n;;
    esac

    read -p "Enable AP? (y/n) " yn
    case $yn in
        [yY] ) init_ap;;
    esac

    echo "[global]

$conf_n2n
" > /root/config.ini
}

if [ -f /root/config.ini ]; then
    read -p "Module config found. Re-initialize? (y/n) " yn

    case $yn in
        [yY] ) initialize_config;
            break;;
	* ) echo invalid response;; 
    esac
else
    initialize_config
fi


echo "[i] Installing deps"
DEBIAN_FRONTEND=noninteractive apt -q install vim tmux htop git whois curl python3-distutils python3-dev libffi-dev rustc libssl-dev -y;
echo "[i] Upgrading local system before install"
# DEBIAN_FRONTEND=noninteractive apt -q -y upgrade;

#curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py
#sudo python3 /tmp/get-pip.py 
#sudo python3 -m pip install -U ansible
sudo apt install -y ansible

sudo touch /root/.init

sudo sh -c 'ansible-pull -U https://gitlab.com/rynojvr/ansible.git  | tee /var/log/provision.log'

echo "Please reboot to finish installation, and enable IP routing"

