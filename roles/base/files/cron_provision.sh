#!/bin/bash
###############################
#   FILE MANAGED BY ANSIBLE   #
# CHANGES WILL BE OVERWRITTEN #
###############################

# Check if ansible-pull is already running, and if not, run it
if pgrep -f ansible-pull; then
    printf "\n$(date +"%Y-%m-%d %H:%M:%S") A running ansible-pull process was found.\nExiting.\n"
else
    ansible-pull -o -U https://gitlab.com/rynojvr/ansible.git 2>&1 > /var/log/provision.log.$(date +%F_%R)
fi
