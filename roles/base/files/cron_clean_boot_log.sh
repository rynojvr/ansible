#!/bin/bash
###############################
#   FILE MANAGED BY ANSIBLE   #
# CHANGES WILL BE OVERWRITTEN #
###############################

[[ $(ls -1r /var/log/boot.log.* | wc -l ) -gt 10 ]] \
    && ls -1r /var/log/boot.log.* | tail -n +10 | xargs rm