#!/bin/bash
###############################
#   FILE MANAGED BY ANSIBLE   #
# CHANGES WILL BE OVERWRITTEN #
###############################

grep -l "Repository has not changed" /var/log/provision.log*  | xargs rm
grep -l "Could not resolve" /var/log/provision.log*  | xargs rm